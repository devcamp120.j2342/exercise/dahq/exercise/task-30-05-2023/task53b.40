import model.Person;
import model.Staff;
import model.Student;

public class App {
    public static void main(String[] args) throws Exception {
        Person person1 = new Person("Nguyen van A", "Can Tho");
        Person person2 = new Person("Nguyen Van B", "Sg");
        System.out.println("Person1: " + person1.toString());
        System.out.println("Person2: " + person2.toString());

        Student student1 = new Student(person1.getName(), person1.getAddress(), "Lop12", 2020, 20.8);
        Student student2 = new Student(person2.getName(), person2.getAddress(), "Lop11", 2019, 29.7);
        System.out.println("student1: " + student1.toString());
        System.out.println("student2: " + student2.toString());

        Staff staff1 = new Staff(person1.getName(), person1.getAddress(), "A", 90.8);
        Staff staff2 = new Staff(person2.getName(), person2.getAddress(), "B", 1000.8);
        System.out.println("Staff1: " + staff1.toString());
        System.out.println("Staff2: " + staff2.toString());
    }
}
